import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          title: Text("Dice Game"),
          backgroundColor: Colors.yellow,
        ),
        body: DiceGame(),
      ),
    ),
  );
}

class DiceGame extends StatefulWidget {
  @override
  _DiceGameState createState() => _DiceGameState();
}

class _DiceGameState extends State<DiceGame> {
  int leftDice = 1;
  int rightDice = 2;
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    void changDiceFace() {
      setState(() {
        leftDice = Random().nextInt(6) + 1;
        rightDice = Random().nextInt(6) + 1;
      });
    }

    return SafeArea(
      child: Center(
        child: Row(
          children: <Widget>[
            Expanded(
              // flex: 2,
              child: Image.asset('assets/images/$leftDice.png'),
            ),
            Expanded(
              child: Image.asset('assets/images/$rightDice.png'),
            ),
            Row(
              children: <Widget>[
                Column(
                  children: [
                    TextButton(
                      child: Text("Submit"),
                      onPressed: () {
                        changDiceFace();
                      },
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
